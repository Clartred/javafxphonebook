package backend;

import java.awt.*;
import java.awt.event.*;
import java.awt.print.*;
import java.util.ArrayList;

public class HelloWorldPrinter implements Printable, ActionListener {

    ArrayList<Entry> list;

    public HelloWorldPrinter(ArrayList<Entry> list) {
        this.list = list;
    }

    public int print(Graphics g, PageFormat pf, int page) throws
            PrinterException {

        if (page > 0) { /* We have only one page, and 'page' is zero-based */
            return NO_SUCH_PAGE;
        }

        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(pf.getImageableX(), pf.getImageableY());

        /* Now we perform our rendering */
        g.drawString("Name", 50, 20);
        g.drawString("Last Name", 150, 20);
        g.drawString("Phone Number", 275, 20);
        g.drawString("Address", 400, 20);
        g.drawString("City", 500, 20);

        for (int i = 0, y = 50; i < list.size(); i++, y = y + 20) {
            Entry e = list.get(i);
            g.drawString(e.getName(), 50, y);
            g.drawString(e.getLastName(), 150, y);
            g.drawString(e.getPhoneNum(), 275, y);
            g.drawString(e.getAdress(),400, y);
            g.drawString(e.getCityName(), 500, y);
        }

        /* tell the caller that this page is part of the printed document */
        return PAGE_EXISTS;
    }

    public void actionPerformed() {
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setPrintable(this);
        boolean ok = job.printDialog();
        if (ok) {
            try {
                job.print();
            } catch (PrinterException ex) {
              /* The job did not successfully complete */
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}