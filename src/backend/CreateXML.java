package backend;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CreateXML {

    public void createXMLDocument(ArrayList<Entry> list) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();

        // staff elements
        Element staff = doc.createElement("Phonebook");
        doc.appendChild(staff);
        // firstname elements
        for (int i = 0; i < list.size(); i++) {

            Entry e = list.get(i);
            String idString = Integer.toString(e.getId());

            //=====================================
            Element id = doc.createElement("ID");
            Element firstname = doc.createElement("firstName");
            Element lastname = doc.createElement("lastName");
            Element phoneNum = doc.createElement("phoneNumber");
            Element address = doc.createElement("address");
            Element cityName = doc.createElement("cityName");

            id.appendChild(doc.createTextNode((idString)));
            firstname.appendChild(doc.createTextNode(e.getName()));
            lastname.appendChild(doc.createTextNode(e.getLastName()));
            phoneNum.appendChild(doc.createTextNode(e.getPhoneNum()));
            address.appendChild(doc.createTextNode(e.getAdress()));
            cityName.appendChild(doc.createTextNode(e.getCityName()));


            staff.appendChild(id);
            staff.appendChild(firstname);
            staff.appendChild(lastname);
            staff.appendChild(phoneNum);
            staff.appendChild(address);
            staff.appendChild(cityName);
        }
        DateFormat dateFormat = new SimpleDateFormat("dd,MM,yyyy");
        Date date = new Date();

        String fileName = (dateFormat.format(date) + ".xml");
        File file = new File(fileName);

        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(file);

        // Output to console for testing
        // StreamResult result = new StreamResult(System.out);

        transformer.transform(source, result);

        System.out.println("File saved!");

    }
}