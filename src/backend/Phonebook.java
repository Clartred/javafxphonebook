package backend;

import frontend.Table;

import java.sql.SQLException;
import java.util.ArrayList;

public class Phonebook {
    private DBConnection db;
    private ArrayList<Entry> list;
    private Table table;

    public Phonebook(DBConnection db, ArrayList<Entry> list, Table table) {
        this.db = db;
        this.list = list;
        this.table = table;
    }

    /**
     * This method will take parameters and create Entry object out of it.
     * Foward it to the DBConnection method insertIntoDB and after that,
     * forward it to the table method getEntries() which will add it
     * to the Data Base.
     *
     * @param name     of Entry.
     * @param lastName of Entry.
     * @param phoneNum of Entry.
     * @param address  of Entry.
     * @param cityName of Entry.
     */
    public void addEntry(String name, String lastName, String phoneNum, String address, String cityName) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        name = name.toLowerCase();
        lastName = lastName.toLowerCase();
        phoneNum = phoneNum.toLowerCase();
        int usersID = db.insertIntoDB(name, lastName, phoneNum, cityName, address);
        list.add(new Entry(usersID, name, lastName, phoneNum, address, cityName));
        table.getEntries();
    }

    /**
     * This method takes lastName and finds it in Array List.
     * Removes it from array list and forwards entries ID
     * to the DBConnection's method deleteEntry() which
     * will delete it in Data Base.
     *
     * @param id of the entry to be deleted.
     */
    public void deleteEntry(int id) {
        for (int i = 0; i < list.size(); i++) {
            Entry e = list.get(i);
            int entryID = e.getId();
            if (entryID == id) {
                list.remove(e);
                try {
                    db.deleteEntry(entryID);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }
        table.getEntries();
    }

    /**
     * This method will find Entry in Array List and edit it.
     * Also, it will forward it to the DBConnection's method updateEntry()
     * which will edit it in DataBase.
     *
     * @param id        of the Entry to be edited.
     * @param firstName of the Entry to be edited.
     * @param lastName  of the Entry to be edited.
     * @param phoneNum  of the Entry to be edited.
     * @param address   of the Entry to be edited.
     * @param cityName  of the Entry to be edited.
     */
    public void editEntry(int id, String firstName, String lastName, String phoneNum, String address, String cityName) {
        for (int i = 0; i < list.size(); i++) {
            Entry e = list.get(i);
            if (e.getId() == id) {
                e.setName(firstName);
                e.setLastName(lastName);
                e.setPhoneNum(phoneNum);
                e.setAdress(address);
                e.setCityName(cityName);
                try {
                    db.updateEntry(e);
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                } catch (InstantiationException e1) {
                    e1.printStackTrace();
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                }
                table.getEntries();
            }
        }
    }

    public ArrayList<Entry> getList() {
        return list;
    }

    public void setList(ArrayList<Entry> list) {
        this.list = list;
    }
}