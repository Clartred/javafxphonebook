package backend;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;

public class DBConnection {
    private static Connection connection;

    /**
     * This method makes connection with DataBase.
     *
     * @return connection to Data Base.
     */
    public static Connection connect() throws
            SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        if (connection == null) {
            String url = "jdbc:sqlite:Phonebook.db";
            connection = DriverManager.getConnection(url);
            return connection;
        } else
            return connection;
    }

    /**
     * This method will create new Data Base and call createNewTable method.
     */
    public void createNewDataBase() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        String url = "jdbc:sqlite:Phonebook.db";

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
                createNewTable();
            }
        }
    }

    /**
     * This method will create new tables for data base.
     */
    public void createNewTable() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        Connection connection = connect();

        String createTable = "Create table if not exists city (cityTableID integer primary key autoincrement, city_ID int, cityName varchar(30), adress varchar(30))";
        Statement statement = connection.createStatement();
        statement.execute(createTable);

        createTable = "Create table if not exists entry (entryID integer primary key autoincrement, name varchar(30) , lastName varchar(30)," +
                " phoneNum varchar(30),  cityID int, FOREIGN  KEY (cityID) REFERENCES city (city_ID))";
        statement = connection.createStatement();
        statement.execute(createTable);
    }


    /**
     * This method will add new Entry into two tables - Entry and Area.
     * Also, it will return's Entrie's ID to the calling method.
     *
     * @param name     of the Entry.
     * @param lastName of the Entry.
     * @param phoneNum of the Entry.
     * @param adress   of the Entry.
     * @param cityName of the Entry.
     * @return the Entrie's ID.
     */
    public int insertIntoDB(String name, String lastName, String phoneNum, String adress, String cityName) throws ClassNotFoundException,
            SQLException, InstantiationException, IllegalAccessException {
        int cityID = 0;

        Connection connection = this.connect();
        String sql = "INSERT INTO entry(entryID, name, lastName, phoneNum, cityID) VALUES(?,?,?,?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);

        preparedStatement.setString(2, name);
        preparedStatement.setString(3, lastName);
        preparedStatement.setString(4, phoneNum);
        preparedStatement.setInt(5, cityID);
        preparedStatement.executeUpdate();

        sql = "INSERT INTO city (cityTableID, city_ID, adress, cityName) VALUES(?,?,?,?)";
        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(2, cityID);
        preparedStatement.setString(3, adress);
        preparedStatement.setString(4, cityName);
        preparedStatement.executeUpdate();

        String retrieveID = "SELECT entryID from entry where entryID =(SELECT max(entryID) from entry)";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(retrieveID);
        return resultSet.getInt("entryID");
    }

    /**
     * This method will check if Data Base exists and call createNewDataBase
     * method if it doesn't exists.
     */
    public void checkIfDBExists() {
        String path = "Phonebook.db";
        File f = new File(path);
        if (!f.exists()) {
            try {
                createNewDataBase();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            try {
                this.connect();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return;
        }
    }

    /**
     * This method will find entry inside of dataBase(entry and city) and
     * change it.
     *
     * @param e - Entry to be updated.
     */
    public void updateEntry(Entry e) throws
            ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

        String update = "UPDATE entry set name = ?, lastName = ?, phoneNum = ?  WHERE entryID = '" + e.getId() + "'";
        PreparedStatement preparedStatement = connection.prepareStatement(update);
        preparedStatement.setString(1, e.getName());
        preparedStatement.setString(2, e.getLastName());
        preparedStatement.setString(3, e.getPhoneNum());
        preparedStatement.executeUpdate();
        update = "UPDATE city set adress = ?, cityName = ?  WHERE cityTableID = '" + e.getId() + "'";
        PreparedStatement prepStatement = connection.prepareStatement(update);
        prepStatement.setString(1, e.getPhoneNum());
        prepStatement.setString(2, e.getCityName());
        prepStatement.executeUpdate();
    }

    /**
     * This method will delete forwarded entry from DataBase
     * (including entry and city table).
     *
     * @param id - id of the Entry to be deleted.
     */
    public void deleteEntry(int id) throws SQLException {

        String deleteEntry = "DELETE FROM entry where entryID = '" + id + "'";
        String deleteCity = "Delete from city where cityTableID = '" + id + "'";
        PreparedStatement pstmt = connection.prepareStatement(deleteEntry);
        pstmt.executeUpdate();
        PreparedStatement preparedStatement = connection.prepareStatement(deleteCity);
        preparedStatement.executeUpdate();
    }

    /**
     * @param lastName
     * @return entry.
     */
    public Entry retriveEntry(String lastName) throws
            ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        lastName = lastName.toLowerCase();

        String selectAll = "SELECT * FROM entry where lastName = '" + lastName + "'";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(selectAll);
        Entry e = new Entry(resultSet.getInt("entryTabelID"), resultSet.getString("name"),
                resultSet.getString("lastName"), resultSet.getString("phoneNum"),
                resultSet.getString("adress"), resultSet.getString("cityName"));
        return e;
    }

    /**
     * This method will retrieve entry and city tables from Data Base, add them both to separate
     * array list's and after that, merge them into one array list which will be forwarded.
     *
     * @return ObservableList entry(list) which will be added to the TableView in GUI.
     */
    public ObservableList<Entry> retriveEntries() throws
            ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        ArrayList<Entry> listOne = new ArrayList<>(), listTwo = new ArrayList<>();
        String selectEntry = "SELECT * FROM entry";
        String selectCity = "SELECT * FROM city";

        try (
                Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery(selectEntry)) {

            while (rs.next()) {
                listOne.add(new Entry(rs.getInt("entryID"), rs.getString("name"),
                        rs.getString("lastName"), rs.getString("phoneNum")));
            }
            try (Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery(selectCity)) {

                while (resultSet.next()) {
                    listTwo.add(new Entry(resultSet.getString("adress"), resultSet.getString("cityName")));
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        ObservableList<Entry> entry = FXCollections.observableArrayList();

        for (int i = 0; i < listOne.size(); i++) {
            Entry e = listOne.get(i);
            Entry x = listTwo.get(i);
            entry.add(new Entry(e.getId(), e.getName(), e.getLastName(), e.getPhoneNum(), x.getAdress(), x.getCityName()));
        }
        return entry;
    }
}