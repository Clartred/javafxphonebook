package backend;

public class Entry {

    private String name, lastName, phoneNum, adress, cityName;
    private int id;

    public Entry(int id, String name, String lastName, String phoneNum, String adress, String cityName) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.phoneNum = phoneNum;
        this.cityName = cityName;
        this.adress = adress;
    }

    public Entry(int id, String name, String lastName, String phoneNum) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.phoneNum = phoneNum;
    }

    public Entry(String adress, String cityName) {
        this.adress = adress;
        this.cityName = cityName;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}