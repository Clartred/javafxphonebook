package frontend;

import backend.DBConnection;
import backend.Entry;
import backend.Phonebook;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.util.ArrayList;

public class MainWindow extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        DBConnection dbConnection = new DBConnection();
        dbConnection.checkIfDBExists();

        ArrayList<Entry> list = new ArrayList<>();

        Table table = new Table(list, dbConnection);
        table.getDBEntries();



        Phonebook phonebook = new Phonebook(dbConnection, list, table);
        TextFields tf = new TextFields();

        Buttons b = new Buttons(tf, phonebook, table);
        BorderPane rootContainer = new BorderPane();

        rootContainer.setTop(b.getButtonGridPane());
        rootContainer.setBottom(tf.getTfGrid());
        rootContainer.setCenter(table.getGridPane());

        rootContainer.setStyle("-fx-font: 22 arial; -fx-base: #609bff;");
        Scene mainScene = new Scene(rootContainer, 700, 700);
        primaryStage.setTitle("Phonebook");
        primaryStage.setScene(mainScene);
        primaryStage.show();
    }
}