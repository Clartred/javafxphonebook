package frontend;

import javafx.geometry.Insets;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class TextFields {
    private TextField firstNameTextField;
    private TextField lastNameTextField;
    private TextField phoneNumberTextField;
    private TextField addressTextField;
    private TextField cityNameTextField;

    private GridPane tfGrid = new GridPane();

    public TextFields() {

        //creating text fields for our inputs into phonebook
        this.firstNameTextField = new TextField();
        this.lastNameTextField = new TextField();
        this.phoneNumberTextField = new TextField();
        this.addressTextField = new TextField();
        this.cityNameTextField = new TextField();

        //Creating Default texts for our text fields
        this.firstNameTextField.setPromptText("Enter First Name");
        this.lastNameTextField.setPromptText("Enter Last Name");
        this.phoneNumberTextField.setPromptText("Enter Phone Number");
        this.addressTextField.setPromptText("Enter your addressTextField");
        this.cityNameTextField.setPromptText("Enter your City Name");

        //positioning our Text Fields
        GridPane.setConstraints(this.firstNameTextField, 0, 7);
        GridPane.setConstraints(this.lastNameTextField, 4, 7);
        GridPane.setConstraints(this.phoneNumberTextField, 6, 7);
        GridPane.setConstraints(this.addressTextField, 0, 10);
        GridPane.setConstraints(this.cityNameTextField, 4, 10);


        //seting minimum size (x,y) for our text fields
        this.firstNameTextField.setMinSize(30, 40);
        this.lastNameTextField.setMinSize(30, 40);
        this.phoneNumberTextField.setMinSize(30, 40);
        this.addressTextField.setMinSize(30, 40);
        this.cityNameTextField.setMinSize(30, 40);

        tfGrid.setPadding(new Insets(10, 10, 10, 10));
        tfGrid.setVgap(10);
        tfGrid.setHgap(10);

        tfGrid.getChildren().addAll(firstNameTextField, lastNameTextField, phoneNumberTextField, addressTextField, cityNameTextField);
    }

    public TextField getFirstNameTextField() {
        return firstNameTextField;
    }

    public TextField getLastNameTextField() {
        return lastNameTextField;
    }

    public TextField getPhoneNumberTextField() {
        return phoneNumberTextField;
    }

    public GridPane getTfGrid() {
        return tfGrid;
    }

    public TextField getAddressTextField() {
        return addressTextField;
    }

    public TextField getCityNameTextField() {
        return cityNameTextField;
    }

}