package frontend;

import backend.DBConnection;
import backend.Entry;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

import java.sql.SQLException;
import java.util.ArrayList;

public class Table {

    private GridPane gridPane;
    private ArrayList<Entry> arrayList;
    private TableView table;
    DBConnection dbConnection;


    public Table(ArrayList<Entry> arrayList, DBConnection dbConnection) {
        gridPane = new GridPane();
        this.arrayList = arrayList;
        this.dbConnection = dbConnection;
    }

    /**
     * This method will fill the whole table with data from array list.
     *
     * @param entry
     */
    public void fillTables(ObservableList<Entry> entry) {

        table = new TableView();
        table.getSelectionModel().setCellSelectionEnabled(true);

        // ======================TRYING TO MAKE TABLE VIEW============================
        TableColumn<String, String> nameColumn = new TableColumn<>("First Name");
        TableColumn<String, String> lastNameColumn = new TableColumn<>("Last Name");
        TableColumn<String, String> phoneNumColumn = new TableColumn<>("Phone Number");
        TableColumn<String, String> addressColumn = new TableColumn<>("Address");
        TableColumn<String, String> cityColumn = new TableColumn<>("City");

        ///=============== SET MIN WIDTH ==================
        nameColumn.setMinWidth(125);
        lastNameColumn.setMinWidth(125);
        phoneNumColumn.setMinWidth(125);
        addressColumn.setMinWidth(125);
        cityColumn.setMinWidth(125);

        nameColumn.setStyle("-fx-font: 22 arial; -fx-base: #67494f;");
        lastNameColumn.setStyle("-fx-font: 22 arial; -fx-base: #67494f;");
        phoneNumColumn.setStyle("-fx-font: 22 arial; -fx-base: #67494f;");
        addressColumn.setStyle("-fx-font: 22 arial; -fx-base: #67494f;");
        cityColumn.setStyle("-fx-font: 22 arial; -fx-base: #67494f;");


        // =================== name of the variable ===================
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        phoneNumColumn.setCellValueFactory(new PropertyValueFactory<>("phoneNum"));
        addressColumn.setCellValueFactory(new PropertyValueFactory<>("adress"));
        cityColumn.setCellValueFactory(new PropertyValueFactory<>("cityName"));


        table = null;
        table = new TableView<>();
        table.setItems(null);
        table.setItems(entry);
        table.getColumns().addAll(nameColumn, lastNameColumn, phoneNumColumn, addressColumn, cityColumn);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        GridPane.setConstraints(table, 4, 4);
        gridPane.getChildren().addAll(table);
    }

    /**
     * This method will retrieve data from Database.
     */
    public void getDBEntries() {
        ObservableList<Entry> list = null;
        try {
            list = dbConnection.retriveEntries();
            fillTables(list);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < list.size(); i++) {
            arrayList.add(list.get(i));
        }
    }

    /**
     * this method will send data to fillTables method.
     */
    public void getEntries() {

        ObservableList<Entry> entry = FXCollections.observableArrayList();

        for (int i = 0; i < arrayList.size(); i++) {
            Entry e = arrayList.get(i);
            entry.add(new Entry(e.getId(), e.getName(), e.getLastName(), e.getPhoneNum(), e.getAdress(), e.getCityName()));
        }
        this.fillTables(entry);
    }


    /**
     * This method will alow user to filter last Names and
     * it will show them inside of table.
     * When he clicks the button again, it will show him
     * normal table.
     * @param stringFilter string to be filtered.
     * @param isTrue - wether button is clicked on unclicked.
     */
    public void filter(String stringFilter, boolean isTrue) {
        ArrayList<Entry> list = new ArrayList<>();
        ObservableList<Entry> entry = FXCollections.observableArrayList();

        if (isTrue == true) {
            for (int i = 0; i < arrayList.size(); i++) {
                if ((arrayList.get(i).getLastName().matches("(.*)" + stringFilter + "(.*)"))) {
                    list.add(arrayList.get(i));
                    entry.add(new Entry(arrayList.get(i).getId(), arrayList.get(i).getName(), arrayList.get(i).getLastName(),
                            arrayList.get(i).getPhoneNum(), arrayList.get(i).getAdress(), arrayList.get(i).getCityName()));
                }
            }
            this.fillTables(entry);
        } else if (isTrue == false) {

            for (int i = 0; i < arrayList.size(); i++) {
                Entry e = arrayList.get(i);
                entry.add(new Entry(e.getId(), e.getName(), e.getLastName(), e.getPhoneNum(), e.getAdress(), e.getCityName()));
            }
            this.fillTables(entry);
        }
    }

    public GridPane getGridPane() {
        return gridPane;
    }

    public void setGridPane(GridPane gridPane) {
        this.gridPane = gridPane;
    }

    public TableView getTable() {
        return table;
    }

    public void setTable(TableView table) {
        this.table = table;
    }
}