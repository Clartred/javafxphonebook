package frontend;

import backend.CreateXML;
import backend.Entry;
import backend.HelloWorldPrinter;
import backend.Phonebook;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.sql.SQLException;

public class Buttons implements EventHandler<ActionEvent> {

    private TextFields fields;
    private Button btnInsert;
    private Button btnEdit;
    private Button btnDelete;
    private Button btnPrint;
    private Button btnExit;
    private ToggleButton btnFilter;
    private GridPane buttonGridPane = new GridPane();
    private Phonebook phonebook;
    private Table table;

    public Buttons(TextFields fields, Phonebook phonebook, Table table) {

        this.phonebook = phonebook;
        this.fields = fields;
        this.table = table;

        //Creating Buttons we need.
        btnInsert = new Button("Insert");
        btnEdit = new Button("Edit");
        btnDelete = new Button("Delete");
        btnPrint = new Button("Print");
        btnExit = new Button("Exit");
        btnFilter = new ToggleButton("Filter");

        //creating Set on Action
        btnInsert.setOnAction(this);
        btnEdit.setOnAction(this);
        btnDelete.setOnAction(this);
        btnPrint.setOnAction(this);
        btnExit.setOnAction(this);
        btnFilter.setOnAction(this);

        //Positioning buttons inside of grid pane(and by doing that, we also position it inside of Border Pane)
        GridPane.setConstraints(btnInsert, 1, 0);
        GridPane.setConstraints(btnEdit, 5, 0);
        GridPane.setConstraints(btnDelete, 9, 0);
        GridPane.setConstraints(btnPrint, 12, 0);
        GridPane.setConstraints(btnFilter, 15, 0);
        GridPane.setConstraints(btnExit, 18, 0);

        //giving colors to our buttons
        btnInsert.setStyle("-fx-font: 22 arial; -fx-base: #0f9b29;");
        btnEdit.setStyle("-fx-font: 22 arial; -fx-base: #724011;");
        btnDelete.setStyle("-fx-font: 22 arial; -fx-base: #86a50a;");
        btnPrint.setStyle("-fx-font: 22 arial; -fx-base: #0063ac;");
        btnExit.setStyle("-fx-font: 22 arial; -fx-base: #761214;");
        btnFilter.setStyle("-fx-font: 22 arial; -fx-base: #405054;");

        buttonGridPane.setPadding(new Insets(10, 10, 10, 10));
        buttonGridPane.setVgap(10);
        buttonGridPane.setHgap(10);

        buttonGridPane.getChildren().addAll(this.btnInsert, this.btnEdit, this.btnDelete,
                this.btnPrint, this.btnExit, btnFilter);
    }

    @Override
    public void handle(ActionEvent event) {

        if (event.getSource().equals(btnExit)) {
            CreateXML createXML = new CreateXML();
            try {
                createXML.createXMLDocument(phonebook.getList());
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (TransformerException e) {
                e.printStackTrace();
            }
            System.exit(0);

        } else if (event.getSource().equals(btnInsert)) {
            if (fields.getFirstNameTextField().getText() != null && fields.getFirstNameTextField().getText().isEmpty() &&
                    fields.getFirstNameTextField().getText().equals("")) {
                return;
            } else if (fields.getLastNameTextField().getText() != null && fields.getLastNameTextField().getText().isEmpty() &&
                    fields.getLastNameTextField().getText().equals("")) {
                return;
            } else if (fields.getPhoneNumberTextField().getText() != null && fields.getPhoneNumberTextField().getText().isEmpty() && fields.getPhoneNumberTextField().getText().equals("")) {
                return;
            } else if (fields.getAddressTextField().getText() != null && fields.getAddressTextField().getText().isEmpty() &&
                    fields.getAddressTextField().getText().equals("")) {
                return;
            } else if (fields.getCityNameTextField().getText() != null && fields.getCityNameTextField().getText().isEmpty() &&
                    fields.getCityNameTextField().getText().equals("")) {
                return;
            } else {
                String firstName = fields.getFirstNameTextField().getText().toLowerCase();
                String lastName = fields.getLastNameTextField().getText().toLowerCase();
                String phoneNum = fields.getPhoneNumberTextField().getText().toLowerCase();
                String address = fields.getAddressTextField().getText().toLowerCase();
                String cityName = fields.getCityNameTextField().getText().toLowerCase();
                try {
                    phonebook.addEntry(firstName, lastName, phoneNum, address, cityName);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                fields.getFirstNameTextField().clear();
                fields.getLastNameTextField().clear();
                fields.getPhoneNumberTextField().clear();
                fields.getAddressTextField().clear();
                fields.getCityNameTextField().clear();
            }

        } else if (event.getSource().equals(btnDelete)) {
            ObservableList<Entry> tableList = table.getTable().getSelectionModel().getSelectedItems();
            Entry e = tableList.get(0);
            phonebook.deleteEntry(e.getId());

        } else if (event.getSource().equals(btnEdit)) {
            ObservableList<Entry> tableList = table.getTable().getSelectionModel().getSelectedItems();
            int id = tableList.get(0).getId();
            String firstName, lastName, phoneNum, address, areaName;

            if (fields.getFirstNameTextField().getText() != null && fields.getFirstNameTextField().getText().isEmpty() &&
                    fields.getFirstNameTextField().getText().equals("")) {
                firstName = tableList.get(0).getName();
            } else {
                firstName = fields.getFirstNameTextField().getText();
            }
            if (fields.getLastNameTextField().getText() != null && fields.getLastNameTextField().getText().isEmpty() &&
                    fields.getLastNameTextField().getText().equals("")) {
                lastName = tableList.get(0).getLastName();
            } else {
                lastName = fields.getLastNameTextField().getText();
            }
            if (fields.getPhoneNumberTextField().getText() != null && fields.getPhoneNumberTextField().getText().isEmpty() && fields.getPhoneNumberTextField().getText().equals("")) {
                phoneNum = tableList.get(0).getPhoneNum();
            } else {
                phoneNum = fields.getPhoneNumberTextField().getText();
            }
            if (fields.getAddressTextField().getText() != null && fields.getAddressTextField().getText().isEmpty() &&
                    fields.getAddressTextField().getText().equals("")) {
                address = tableList.get(0).getAdress();
            } else {
                address = fields.getAddressTextField().getText();
            }
            if (fields.getCityNameTextField().getText() != null && fields.getCityNameTextField().getText().isEmpty() &&
                    fields.getCityNameTextField().getText().equals("")) {
                areaName = tableList.get(0).getCityName();
            } else {
                areaName = fields.getCityNameTextField().getText();
            }
            phonebook.editEntry(id, firstName.toLowerCase(), lastName.toLowerCase(), phoneNum.toLowerCase(), address.toLowerCase(), areaName.toLowerCase());
            fields.getFirstNameTextField().clear();
            fields.getLastNameTextField().clear();
            fields.getPhoneNumberTextField().clear();
            fields.getAddressTextField().clear();
            fields.getCityNameTextField().clear();

        } else if (event.getSource().equals(btnPrint)) {
            HelloWorldPrinter helloWorldPrinter = new HelloWorldPrinter(phonebook.getList());
            helloWorldPrinter.actionPerformed();

        } else if (event.getSource().equals(btnFilter)) {
            if (btnFilter.isSelected()) {
                if (fields.getLastNameTextField().getText() != null && fields.getLastNameTextField().getText().isEmpty() &&
                        fields.getLastNameTextField().getText().equals("")) {
                    return;
                } else {
                    table.filter(fields.getLastNameTextField().getText(), true);
                }
            }
            if (!btnFilter.isSelected()) {
                table.filter(" ", false);
            }
        }
    }

    public GridPane getButtonGridPane() {
        return buttonGridPane;
    }

    public void setButtonGridPane(GridPane buttonGridPane) {
        this.buttonGridPane = buttonGridPane;
    }

}