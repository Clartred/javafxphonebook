import frontend.MainWindow;
import javafx.application.Application;

import java.sql.SQLException;

public class Main {

    public static void main(String args[]) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

        Application.launch(MainWindow.class);
    }
}

//bagljas, duvanika, zeleno polje, mala amerika, muzlja, zitni trg, contika